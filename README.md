## Ubuntu Desktop (GNOME 3) Dockerfile

将此镜像clone到自己的目录，再进入此目录。

注意：start.sh和xstartup都需要执行权限，直接先

>chmod 777 xstartup
>chmod 777 start.sh

### 构建镜像：
> docker build -t u20ult .

注意后面的那一个点，是英文的句号，在linux里表示当前目录。


### 运行：
> docker run -d --tmpfs /run --tmpfs /run/lock --tmpfs /tmp -v /sys/fs/cgroup:/sys/fs/cgroup --env Width=1908 --env Height=940 -p 5901:5901 -p 6901:6901 u20ult

这里可以通过 --evn 指定屏幕宽度高度


